from django.urls import path

from . import views

app_name = 'landing'

urlpatterns = [
    path('', views.home, name='home'),
    path('pollsudah', views.pollSudah, name='sudah'),
    path('pollbelum', views.pollBelum, name='belum'),
    path('hasilpoll', views.hasilPoll, name='hasilPoll'),
]
