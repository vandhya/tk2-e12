from django.shortcuts import render, redirect
from django.http import JsonResponse
from django.http import HttpResponse
from .models import PollModel
from django.contrib.auth.decorators import login_required
import json

def home(request):
    if request.user.is_authenticated:
        punya_usernya = PollModel.objects.filter(poll_user=request.user).count()
    else:
        punya_usernya = 0

    vote = {"punya_usernya": punya_usernya}
    return render(request, 'landing/home.html', vote)

def hasilPoll(request):
    sudah = PollModel.objects.filter(poll_type='sudah').count()
    belum = PollModel.objects.filter(poll_type='belum').count()

    count = {
        'sudah' : sudah,
        'belum' : belum,
    }
    return JsonResponse(count, safe=False)

def pollSudah(request):
    tambah = PollModel.objects.create(poll_type='sudah', poll_user=request.user)
    tambah.save()
    return JsonResponse({'vote': 'sudah'}, safe=False)

def pollBelum(request):
    tambah = PollModel.objects.create(poll_type='belum', poll_user=request.user)
    tambah.save()
    return JsonResponse({'vote': 'belum'}, safe=False)