from django.test import TestCase, Client
from .forms import *

class TestCaseLogin(TestCase):
    def test_url_register(self):
        responses = Client().get("/newuser/")
        self.assertEquals(responses.status_code, 200)
    
    def test_template_register(self):
        responses = Client().get("/newuser/")
        self.assertTemplateUsed(responses, 'login/register.html')
    
    def test_create_user(self):
        user = {
            'first_name' : 'kak',
            'last_name' : 'pewe',
            'username' : 'ppw',
            'email' : 'kakpewe@ppw.com',
            'password1' : 'akuuserbaru',
            'password2' : 'akuuserbaru',
        }

        responses = self.client.post('/newuser/', user)
        testuser = User.objects.get(email = 'kakpewe@ppw.com')
        self.assertEqual(testuser.username, 'ppw')
        self.assertEqual(testuser.first_name, 'kak')
        self.assertEqual(testuser.last_name, 'pewe')

    def test_url_login(self):
        responses = Client().get("/login/")
        self.assertEquals(responses.status_code, 200)
    
    def test_template_login(self):
        responses = Client().get("/login/")
        self.assertTemplateUsed(responses, 'login/login.html')

    def test_url_logout(self):
        responses = Client().get("/logout/")
        self.assertEquals(responses.status_code, 200)

    def test_template_logout(self):
        responses = Client().get("/logout/")
        self.assertTemplateUsed(responses, 'login/logout.html')

   

    
        

