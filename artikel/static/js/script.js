$(document).ready(function() {

    $(".blog-entry").mouseenter(function() {
        $(this).css({border: '0 solid #14274E'}).animate({
            borderWidth: 2, borderRadius:10, padding:5
        }, 100);
    }).mouseleave(function(e) {
        $(this).animate({
            borderWidth: 0
        }, 100);
    });

    $(document).on("click", ".del_btn" , function(e) {
        var r = confirm("Are you sure you want to delete the comment?");
        if (r) {
            console.log($(this).attr("href"));
            var com = $(this).closest(".comment-artikel")
            e.preventDefault();
            $.ajax({
                type:'POST',
                url: $(this).attr("href"),
                success: function(json) {
                    com.remove();
                    console.log(json);
                }
            });
        }
    });

    $("#form-comment").submit(function (e) {
        e.preventDefault();
        var serializedComment = $(this).serialize();
        console.log("create post is working!")
        $.ajax({
            type: 'POST',
            url:$(this).attr('action'),
            data: serializedComment,
            success: function (json) {
                // clear form
                var htmlcomment = $(".list-comment");
                console.log(json);

                var writer = json[0].author
                var body = json[0].body
                var date = json[0].date
                var id = json[0].id
                $("#form-comment").trigger('reset');
                htmlcomment.prepend(
                    "<div class='comment-artikel col'>" +
                    "<button " + 
                        "href=" + "\"" + "/artikel/delete/" + id + "/\"" +
                        "style='float: right;padding: 5px; margin-top: 5px;'" + 
                        "type='button'" +
                        "class='del_btn btn btn-outline-danger'>" +
                        "x</button>" + 
                    "<h4 style='display: inline-block;margin-top: 5px;'>" + writer + " •</h4>" +
                    "<p style='display: inline-block;margin-left: 20px;'>" + date + "</p>" +
                    "<p>" + body + "</p>" +
                    "</div>"
                );

                
            },
            error: function(response,e) {
                console.log("error");
            },
        });
    });

});
function getCookie(c_name)
{
    if (document.cookie.length > 0)
    {
        c_start = document.cookie.indexOf(c_name + "=");
        if (c_start != -1)
        {
            c_start = c_start + c_name.length + 1;
            c_end = document.cookie.indexOf(";", c_start);
            if (c_end == -1) c_end = document.cookie.length;
            return unescape(document.cookie.substring(c_start,c_end));
        }
    }
    return "";
 }

$(function () {
    $.ajaxSetup({
        headers: { "X-CSRFToken": getCookie("csrftoken") }
    });
});