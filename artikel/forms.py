from django import forms
from . import models

class TulisArtikel(forms.ModelForm):
    class Meta:
        model = models.Artikel
        fields = ['judul','isi']

    error_messages = {
        'required' : 'Please type'
    }

    judul_attrs = {
        'type' : 'text',
        'placeholder' : 'Judul artikel'
    }

    isi_attrs = {
        'type' : 'text',
        'placeholder' : 'Text artikel ...'
    }

    judul = forms.CharField(label='Judul artikel:', required=True, max_length=150, widget=forms.TextInput(attrs=judul_attrs))
    isi = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=isi_attrs))

class TulisComment(forms.ModelForm):
    class Meta:
        model = models.Comment
        fields = ['body']
    body_attrs = {
        'type' : 'text',
        'placeholder' : 'Write a comment on this article',
        'style' :'width: 100%;margin-bottom:10px;margin-right:20px;',
    }

    body = forms.CharField(label='', required=True, max_length=300, widget=forms.TextInput(attrs=body_attrs))