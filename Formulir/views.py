from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import covidTest
from .forms import formTest
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
import json

@login_required(login_url='/login/')
def formPage(request):
    formulir = formTest()
    if request.method == "POST":
        formulir = formTest(request.POST)
        if formulir.is_valid():
            formulir.save()
            return render(request, 'respon.html', {})
            # return JsonResponse(load_data, safe=False)
            # return JsonResponse({'message': 'success'})
        else:
            messages.error(request, " ")
    
    context = {
        'form' : formulir
    }
    return render(request, 'formulir.html', context)

def Data(request):
    dict_data = '[{"id":"%s", "Nama_Lengkap":"%s", "Usia":"%s", "No_Telp":"%s", "Jenis_Kelamin":"%s", "Alamat":"%s", "Nama_Test":"%s", "Lokasi_Test":"%s"}]' %(form.id, form.Nama_Lengkap, form.Usia, form.No_Telp ,form.Jenis_Kelamin, form.Alamat, form.Nama_Test, form.Lokasi_Test)
    load_data = json.loads(dict_data)
    # return render(request, 'respon.html', {})
    return JsonResponse(load_data, safe=False)

@login_required(login_url='/login/')
def listForm(request):
    list_formulir = covidTest.objects.all()
    context = {'listForm':list_formulir}
    return render(request, 'list.html', context)

def cariForm(request):
    cari = request.GET['q']
    cekContain = covidTest.objects.filter(Nama_Lengkap__icontains=cari) | covidTest.objects.filter(Usia__icontains=cari) | covidTest.ojects.filter(No_telp__icontains=cari) | covidTest.objects.filter(Jenis_Kelamin__icontains=cari) | covidTest.objects.filter(Alamat__icontains=cari) | covidTest.objects.filter(Nama_test__icontains=cari) | covidTest.objects.filter(Lokasi_Test__icontains=cari)
    data = cekContain.values()
    return JsonResponse(data, safe=False)