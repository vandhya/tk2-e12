from django.urls import path
from . import views

app_name = "Formulir"
urlpatterns = [
    path('', views.formPage, name="formulir"),
    path('listForm/', views.listForm, name="listForm"),
    path('cariForm/', views.cariForm, name="cariForm"),
    path('Data/', views.Data, name="Data"),
]